#!/usr/bin/python

import os.path

s = 0

for i in os.listdir(os.getcwd()):
    if i.lower().endswith(('.py','.txt','.html','.png','.css')) or '.' not in i:
        s += 1
    else:
        s *= 0

if os.path.isfile('about.html') and os.path.isfile('links.html'):
    s += 1
else:
    s *= 0

if s < 1:
    raise Exception("Files are missing or inappropriate.")