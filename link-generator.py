#!/usr/bin/python
# Praecipio Consulting
# All rights reserved, yes that means you, 2015

# HTML CONTENTS, PART 1

contents1 = """<!DOCTYPE html>
<html>
<head>
<title>Demo Website</title>
<link rel="stylesheet" type="text/css" href="sidebar.css">
</head>

<body>

<div id="notfooter">

	<div id="header">

	<img src="stash-wordle.png" width="90%">

	<!-- <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStAAzZAfO1G-MIH9Fr6fbNauvSDFysvp33U3aTHivb3Z98ZMDDCQ"> -->

	<p>Welcome to the wonderful world of web development!</p>

	</div> <!--header-->

	<div id="links">

<ul>
<a href="atlassian-stash.png" target="content"><li>Home</li></a>

"""


# HTML CONTENTS, PART 2

contents2 = """
</ul>

	</div> <!--links-->

</div> <!--notfooter-->

<div id="footer">

	All rights reserved Praecipio Consulting, 2015. 
	Copyright, Patent pending, I pity da'foo enabled

</div> <!--footer-->

</body>

</html>"""


# GETTING THE NAMES OF THE FILES

# os.getcwd() returns string of current directory
# os.listdir() returns error; requires one argument
# os.listdir(os.getcwd()) returns list of all files in that directory

# we're also splitting on the period, returning the 0th in that list, and capitalizing

import os.path

b = ""

for i in os.listdir(os.getcwd()): 
    if i.endswith("links.html") or i.endswith("index.html"):
        pass
    elif i.endswith(".txt") or i.endswith(".html"): 
        b = b + '<a href="' + i + '" target="content"><li>' + i.split('.',1)[0].capitalize() + "</li></a>\n"
        continue
    else:
        continue


# MAKING THE INDEX FILE

contents = contents1 + b + contents2

f = open('links.html','w')

f.write(contents)
f.close()